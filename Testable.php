<?php
	namespace PHPUnit\Framework;
	class Testable
	{
		public $trueProperty = true;
		public $resetMe = true;
		
		public $testArray = Array(
			'first key' => 1,
			'second key' => 2
		);
		
		private $testString = "I do love me some strings";

		public function construct()
		{
		}
		
		public function addValues ($valueOne,$valueTwo){
			return $valueOne+$valueTwo;
		}
		
		public function getTestString(){
			return $this->testString;
		}
	}
?>
