<?php
	namespace PHPUnit\Framework;
	use PHPUnit\Framework\TestCase;
	include "Testable.php";
	
	class TestableTest1 extends TestCase
	{
		private $_testable = null;
		public function setUp()
		{
			$this->_testable = new Testable();
		}
		public function tearDown()
		{
			$this->_testable = null;
		}
		
		//True or False
		public function testTruePropertyIsTrue()
		{
			$this->assertTrue($this->_testable->trueProperty,"trueProperty isn't false");
		}
	}
	
?>
